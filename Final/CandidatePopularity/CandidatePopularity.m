% a sample structure array to store the credentials
creds = struct('ConsumerKey','81yRct2agF6YFc3iaiAmQFtXd',...
    'ConsumerSecret','6tBiQCtfCIR411Yn1QitthGPwcSz53k1gsCPOzkJHhMkTN4iYY',...
    'AccessToken','742443086199033856-Nu46QuMlNQX0jKvyKNSJEZ0RhEZaf7a',...
    'AccessTokenSecret','eRaDmzUsl9pZJlohCHuOQ4mHwgzVvOdfU7Xdw09adtPgh');

% set up a Twitty object
addpath twitty_1.1.1; % Twitty
addpath parse_json; % Twitty's default json parser
addpath jsonlab; % I prefer JSONlab, however.
load('creds.mat') % load my real credentials
tw = twitty(creds); % instantiate a Twitty object
tw.jsonParser = @loadjson; % specify JSONlab as json parser

% search for English tweets that mention 'amazon' and 'hachette'
jhh = tw.search('Andr�s Manuel L�pez Obrador','count',100,'include_entities','true','lang','en');
tpm = tw.search('hachette','count',100,'include_entities','true','lang','en');
maf = tw.search('amazon hachette','count',100,'include_entities','true','lang','en');
ind1 = tw.search('hachette','count',100,'include_entities','true','lang','en');
ind2 = tw.search('amazon hachette','count',100,'include_entities','true','lang','en');