clear

%leer un archivo csv (Comma Separated Values)
filename='atus2015mod.csv';
Casos = csvread( filename );
probCV = zeros(3, 5);

%Cross Validation de 5 etapas
cv = 1;
while(cv<5)
    
    [Train, Test] = Methods.crossValidation(Casos, cv);
    
    %numero de casos y numero de atributos
    S = size(Train);
    %Clases que hay en en la base de datos
    classes = unique(Train(:, 43));
    
    %arreglo que contiene los diferentes valore que se pueden encontrar en la
    %matriz de Training de la columna 1 a la 42 (atributos, columna 43 es la clase)
    uniqueVal = unique(Train(:,1:42));
    
    results = zeros(size(uniqueVal, 1), S(2), size(classes, 1));

%********************************Training**********************************

    aPriori = zeros(size(classes, 1));
    
    for i = 1 : size(classes, 1)
        %datos separados por clase (campo 43 es 1, 2 � 3)
        map = Train(:, 43) == classes(i);
        data = Train(map, 1:42);

        %----------------Probabilidad a Priori de cada clase----------------
        aPriori(i) = Methods.calcAPriori(map, S);

        %-----------------------Frecuencias por clase-----------------------
        %matrices de valores unicos() por atributos(42)
        count = Methods.countFreq(uniqueVal, S, data);

        %-----------------------Correccion de Laplace-----------------------
        %suma uno (+1) a cada casilla de la matriz
        laplace = count + 1;

        %----------------------------Normalizar-----------------------------
        %matriz obtenida es dividida entre el total de valores que se pueden
        %obtener en una columna
        norm = laplace / sum(laplace(:,1));

        %--------------------------Probabilidades---------------------------
        %concatena el valor unico con sus probabilidades en cada atributo
        results(:, :, i) = [uniqueVal, norm];
    end;

    
    
    %******************************Testing******************************

    
    %probabilidades de un registro (1 campo prob a priori, 42 campos prob de
    %cada atributo, 1 campo prob total
        estimatedProb = zeros (S(2)+1, size(classes, 1));
    
    probs = zeros(size(Test, 1), size(classes, 1));
    
    for i = 1 : size(classes, 1)

        %loop que va desde el primer registro hasta el ultimo
        for j = 1 : size(Test, 1)

            %iguala el primer campo y el �ltimo a la probabilidad a priori (el
            %ultimo va multiplicando las probabildades
            estimatedProb(end, i) = aPriori(i);
            estimatedProb((size(Test, 1))-1, i) = aPriori(i);

            %loop que calcula los campos de probabilidad en los atributos (campos
            %1 al 42 en el arreglo)
            for k = 1 : 42

                %el valor en la posicion "j" del arreglo es asignado en contrando
                %el en la columna "k" que en su columna 1 (valores unicos) tenga el
                %valor que mandado por el registro de Testing
                val = find(results(:, 1, i)==Test(j,k), 1);
                estimatedProb(i, j) = results(val, k, i);

                %multiplica la prob total por todas las anteriores
                estimatedProb(i, end) = estimatedProb(i, end) * estimatedProb(i, j);
            end;

            probs(j, i) = estimatedProb(i, end);
            
        end;
    end;
    
    cont = Methods.countSucces(size(Test, 1), size(classes, 1), probs);
    
    probCV(cv) = Methods.getSucces(size(classes, 1), Test, cont);

    cv = cv + 1;
end;

