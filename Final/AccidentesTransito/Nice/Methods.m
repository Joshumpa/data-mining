classdef Methods
    
    methods (Static)
        
        %funcion para obtener los datos
        function [Train, Test] = crossValidation(array, i)
            
            s = size(array, 1);
            k = .2 * s;
            a = ((i*k)-k)+1;
            b = i*k;
            
            Test = array(a:b, :);
            
            Train = array;
            Train(a:b, :) = [];
            
        end;
        
        
        function [aPriori] = calcAPriori(map, S)
            aPriori = sum(map)/S(1);
        end;
        
        
        function [count] = countFreq (uniqueVal, S, data)
            
            count = zeros(size(uniqueVal, 1), S(2)-1);

            for j = 1 : size(uniqueVal, 1)
                count(j,:) = sum(data == uniqueVal(j));
            end;
        end;
        
        
        function [counter] = countSucces (Test, classes, probs)
            counter = zeros(classes);
            
            for i = 1 : Test
                [max, index] = max(probs(i,:));
                if(index == Casos(i, 43))
                    counter(index) = counter(index) + 1;
                end;
            end;
        end;
        
        
        function [succes] = getSucces(classes, Test)
            succes = zeros(classes+1, 1);
            
            for i = 1 : classes
                total = sum(Test(:, 43) == classes(i));
                succes(i) = (cont(i) / total) * 100;
                succes(end) = succes(end) + succes(i);
            end
            
            succes(end) = succes(end) / classes;
        end
    end
end