Laboratorios hechos en el curso de Minería de datos.
Codificados en MATLAB.

Lab1: Script de MATLAB en el que se explora un archivo csv sobre enfermedades
del corazón. Se trata de encontrar las principales características de los
sujetos que tienen un diagnóstico positivo (1 al 4) vs los que tienen uno
negativo en problemas del corazón.

Lab2: Análisis sobre casos con cáncer de mama de un estudio realizado en
Wisconsin, mediante el cual los datos fueron sometidos al clasificador ingenuo
de Bayes, entre otros métodos estadísticos.

Lab3: Análisis sobre muestras de diferentes tipos de platas Iris, mediante el
cual los datos fueron sometidos al clasificador K-Means.

Final: Buscar si existe un patrón en el comportamiento de los accidentes de
tránsito, para predecir los daños que puedan ocasionar.
Investigar sobre el comportamientos de los accidentes fatales, no fatales o en
los que sólo hubo daños.
Diferencia accidentes entre hombres y mujeres.

Para información más completa inspeccionar los archivos word o pdf existente en
cada carpeta.
