%***********************Analisis de Atributos***********************

%agregar directorio de b�squeda
addpath 'E:\Trabajos\Mineria de datos\Lab2';

%leer un archivo csv (Comma Separated Values)
filename='breastCancerWisconsinCorrected.csv';
Casos = csvread( filename );

%datos separados por clase (campo 11 es 2 � 4), sin el id y clase
map1 = Casos(:, 11) == 2;
map2 = Casos(:, 11) == 4;
datosC1 = Casos(map1, 2:10);
datosC2 = Casos(map2, 2:10);

%loop para observar el comportamiento de cada atributo
for i=1 : size(datosC1(1 , :),2)
    
    %Media, Desviacion Estandar y Grafica de cada atributo
    mediaC1 = mean(datosC1(:,i));
    mediaC2 = mean(datosC2(:,i));
    desvC1 = std(datosC1(:,i));
    desvC2 = std(datosC2(:,i));
    
    %poner breakpoints para ver cada grafica
    plot (datosC1(:,i), 'mo', 'MarkerEdgeColor', 'b', 'MarkerFaceColor','b');
    plot (datosC2(:,i), 'mo', 'MarkerEdgeColor', 'b', 'MarkerFaceColor','b');

end;

%cargar un archivo con datos ".mat"
filename='CasosTrainTestWisconsin.mat';
load( filename );

%numero de casos y numero de atributos
S=size(Train);

%datos separados por clase (campo 11 es 2 � 4), sin el id y clase
map1 = Train(:, 11) == 2;
map2 = Train(:, 11) == 4;
datosC1 = Train(map1, 2:10);
datosC2 = Train(map2, 2:10);

%*****************************Training******************************

%----------------Probabilidad a Priori de cada clase----------------
probC1 = sum(map1)/S(1);
probC2 = sum(map2)/S(1);

%-----------------------Frecuencias por clase-----------------------
%arreglo que contiene los diferentes valore que se pueden encontrar en la
%matriz de Training de la columna 2 a la 10 (atributos, columna 1 es el id 
%del registro y columna 11 es la clase)
valoresUnicos = unique(Train(:,2:10));
%matrices de 10(valores unicos) por 9(atributos)
conteoC1 = zeros(10, 9);
conteoC2 = zeros(10, 9);

%loop del primer al ultimo valor �nico (1 al 10)
for i = valoresUnicos(1) : valoresUnicos(end)
    %fila "i" es igual a la suma de valores "i" que se encuentre en cada
    %columna (9 en total)
    conteoC1(i,:) = sum(datosC1 == i);
    conteoC2(i,:) = sum(datosC2 == i);
end

%-----------------------Correccion de Laplace-----------------------
%suma uno (+1) a cada casilla de la matriz
laplaceC1 = conteoC1 + 1;
laplaceC2 = conteoC2 + 1;

%----------------------------Normalizar-----------------------------
%matriz obtenida es dividida entre el total de valores que se pueden
%obtener en una columna
normC1 = laplaceC1 / sum(laplaceC1(:,1));
normC2 = laplaceC2 / sum(laplaceC2(:,1));

%--------------------------Probabilidades---------------------------
%concatena el valor unico con sus probabilidades en cada atributo
C1 = [valoresUnicos, normC1];
C2 = [valoresUnicos, normC2];


%******************************Testing******************************

%probabilidades de un registro (1 campo prob a priori, 9 campos prob de
%cada atributo, 1 campo prob total
probEstimadaC1 = zeros (11, 1);
probEstimadaC2 = zeros (11, 1);

%arreglo que almacena los resultados obtenidos
resultados = zeros(size(Test(:, 1), 1), 2);

%contadores de aciertos
contC1 = 0;
contC2 = 0;

%loop que va desde el primer registro hasta el ultimo
for i = 1 : size(Test(:, 1), 1)
    
    %iguala el primer campo y el �ltimo a la probabilidad a priori (el
    %ultimo va multiplicando las probabildades
    probEstimadaC1([1 11]) = probC1;
    probEstimadaC2([1 11]) = probC2;
    
    %loop que calcula los 9 campos de probabilidad en los atributos (campos
    %2 al 10 en el arreglo)
    for j = 2 : 10
        
        %el valor en la posicion "j" del arreglo es asignado en contrando
        %el en la columna "j" que en su columna 1 (valores unicos) tenga el
        %valor que mandado por el registro de Testing
        probEstimadaC1(j) = C1(Test(i,j), j);
        probEstimadaC2(j) = C2(Test(i,j), j);
        
        %multiplica la prob total por todas las anteriores
        probEstimadaC1(11) = probEstimadaC1(11) * probEstimadaC1(j);
        probEstimadaC2(11) = probEstimadaC2(11) * probEstimadaC2(j);
    end;
    
    %almacenar resultados
    resultados(i, 1) = probEstimadaC1(11);
    resultados(i, 2) = probEstimadaC2(11);
    
    %al obtener las dos probabilidades se pregunta para conocer cu�l es la
    %prob que tuvo valor mayor y se van contando los resultados aciertos
    if (probEstimadaC1(11) > probEstimadaC2(11)) && (Test(i, 11) == 2)
        contC1 = contC1 + 1;
        
    elseif (probEstimadaC2(11) > probEstimadaC1(11)) && (Test(i, 11) == 4)
        contC2 = contC2 + 1;
            
    end;
end;

%total de registros de cada clase(campo 11 es 2 � 4)
totalC1 = sum(Test(:, 11) == 2);
totalC2 = sum(Test(:, 11) == 4);

%calcular la prob de aciertos de cada clase
probAciertosC1 = (contC1 / totalC1) * 100;
probAciertosC2 = (contC2 / totalC2) * 100;

%calcular la prob de aciertos en total
probTotalAciertos = (probAciertosC1 + probAciertosC2) / 2;
