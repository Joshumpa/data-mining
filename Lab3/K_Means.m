clear

%nombre del archivo
filename = 'iris.data';

%llamar a la funcion que convierte los datos
[data, class] = Methods.convertData(filename);

%numero de muestras
m = size( data, 1 );
%numero de atributos
att = size( data, 2 );

%*********************************K-Means**********************************

%especificar numero de clusters
K = 3;

%llamar a una funcion que regresa centroides aleatorios
centroids = Methods.rndcentroids( m, K, att, data );
centroids2 = centroids;
%inicializar con ceros la lista de clusters dados
clusters = zeros ( m, 1 );
%boolean para saber cuando no hay mas centroides optimos
equal = false;


while equal ~= true
    
    %loop que recorre cada muestra
    for i = 1 : m

        %llama a la funcion que calcula la distacia
        distance = Methods.distance( K, data(i, :), centroids );
        %guarda la minima distancia
        minimum = min (distance);
        %guarda la posicion que contiene la distancia m�s corta
        clusters (i) = find (distance == minimum);

    end
    
    %loop que recorre cada centroide
    for k = 1 : K

        %crea un map con resultados booleanos sobre la existencia de cada
        %cluster
        map = clusters (:) == k;
        
        %cambia los atributos del centroido por la media del cluster
        centroids(k,:) = mean( data(map, :) );

    end
    
    if(size(unique(clusters)) ~= 3)
        centroids = Methods.rndcentroids( m, K, att, data );
    end
    
    %si los centroides son iguales a los anteriores
    if centroids(:,:) == centroids2(:,:)
        %para romper el while
        equal = true;
    else
        %actualiza los centroides para la proxima comparacion
        centroids2 = centroids;
    end
    
end


%**********************Verificar Los Datos Obtenidos***********************

%rango en las muestras que se tiene en cada clase
range = [ 1, 50];
%arreglo que almacena las clases finales
finalClass = zeros( m, 1);
%arreglo que guarda el porcentaje de acierto
succespercent = zeros( K+1, 2);

for k = 1 : K
    %elige el valor que se repite m�s en el rango determinado
    mostfreq = mode(clusters( range(1):range(2), 1));
    %map para detectar las posiciones
    map = clusters(:) == mostfreq;
    %le agrega la clase 'k' a los valores verdaderos
    finalClass( map, 1) = k; 
    
    %map para determinar la clase 'k'
    map1 = (class( range(1):range(2), 1) == k);
    map2 = (finalClass( range(1):range(2), 1) == k);
    %le da el nombre al porcentage
    succespercent (k, 1) = k;
    %calcula el promedio en base a las parecidos entre las predicciones el
    %valor verdadero
    succespercent (k, 2) = mean(map2 == map1)*100;
    
    %va al siguiente rango de datos
    range = range + 50;
end

%porcentaje total de aciertos
succespercent (4, 1) = 4;
succespercent (4, 2) = mean(finalClass == class)*100;
