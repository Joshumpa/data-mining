classdef Methods
    
    methods (Static)
        
        %funcion para obtener los datos
        function [data, class] = convertData(filename)
            %abrir el archivo
            fileID = fopen(filename);
            %escanear el archivo delimitandolo con "," y guardar en vectore seg�n el
            %valor (4 double y 1 string)
            cols = textscan(fileID,'%f %f %f %f %s','delimiter',',');
            %cerrar el archivo
            fclose(fileID);
            %concatenar los vectores con los datos
            data = [cols{1}, cols{2}, cols{3}, cols{4}];
            %guardar las clases, representadas con numeros
            class = cols{5};
            class(strcmp(class,'Iris-setosa')) = {'1'};
            class(strcmp(class,'Iris-versicolor')) = {'2'};
            class(strcmp(class,'Iris-virginica')) = {'3'};
            %convertir a double
            class = str2double(class);
        end
        
        %funcion que regresa un arreglo con centroides aleatorios
        function [cent] = rndcentroids( m, K, att, data )
            %se crea el arreglo con el tama�o del numero de clusters y los
            %atributos que tiene cada muestra
            cent = zeros(K, att);
            
            %loop del numero de clusters
            for k = 1 : K
                
                %loop para cada atributo
                for a = 1 : att
                    
                    %calcula el maximo y minimo del atributo
                    mi = min(data(:, a));
                    ma = max(data(:, a));

                    %guarda un numero aleatorio en el atributo del
                    %centroide
                    cent(k, a) = mi + (ma-mi).*rand(1,1);
                end
            end
        end
    
        %funcion que devuelve un arreglo con las distancias euclidianas
        function [dist] = distance ( K, dataX, centroids )
            %crea un vector del tama�o del numero de clusters
            dist = zeros(K, 1);
            
            %loop que recorre cada cluster
            for k = 1 : K
            
                %dist(k) = sum( abs(dataX - centroids(k)) );
                %distancia = 
                %resta los datos del centroide a los de la muestra
                %los eleva al cuadrado
                %suma cada columna
                %raiz cuadrada
                dist(k) = sqrt( sum( (dataX - centroids(k, :)).^2 ) );
                
            end
        end
    end
end