%Para aprender a manejar matrices
Num_reng = 5;
Num_col = 7;
%inicializar una matriz en ceros
A = zeros(Num_reng, Num_col);
%Transpose A
TA = A';
%Acceso a los datos
A(1,5) = 5;
% Asignar 9 al primer elemento del �ltimo rengl�n
A(5,1) = 9;
%Todo el segundo rengl�n tenga el valor 2
A(2,:) = 2;
%La columna 3 que tenga el valor 23
A(:,3) = 23;
%sumar un elemento a una matriz
B = A + 2;
%Sumar 2 matrices
C = A + B;
%Mult Matrices
%Para ver los distintos valores que contiene una matriz
U = unique(A);
MapCeros = (A==2);
%Suma de los 1 en el segundo rengl�n
SumUnos = sum(MapCeros(2,:));
%Tama�o de la matriz
S = size(A);
%S(1) es el num de renglones de A y S(2) es el num de cols
%Ejercicio: Sacar la tabla de frecuencia de los distintos valores de la matriz
SU = size(U);%numero de diferentes valores en el arreglo
Freqs = zeros(SU);%arreglo unidimensional del tama�o de SU
%Contar la frecuencia de cada valor
for c=1:SU(1)
    map=A==U(c);%cuenta los valores identicos al numero que se encuentra en la posicion "c" del arreglo U
    Freqs(c)=sum(sum(map));%suma todos los numeros positivos en el map
end
Frecuencias = [U , Freqs];%concatena los numeros mas su frecuencia

%-------------------------------- PARTE DOS --------------------------------

%Calcular la media de cada columna de A
Av = mean(A);
% Calcular la desviaci�n est
ds = std(A);

% Filtrar datos:
% Utilice un mapa para que los datos cumplan con un criterio. 
% Ese mapa se puede utilizar como �ndice para c�lculos con los elementos de
% la matriz que hayan cumplido el requisito:

Mapa = A > 1;
A(Mapa);

%Sumar s�lo los elementos que cumplen con el criterio
sum(sum(A(Mapa)));

%Ver archivo HeartDisease
%agregar directorio de b�squeda
addpath 'E:\Trabajos\Mineria de datos'

%Leer un archivo csv (Comma Separated Values)
filename='HeartDisease.csv';
Casos = csvread( filename );

% Eliminar la 1ra columna, que contiene un id para los registros
Casos(:, 1) = [ ];
unique(Casos(:, 58));
Map = Casos(:, 58)==0;
Map2= Casos(:, 58)~=0;
Casos_NoHD = Casos (Map, :);
Casos_SiHD = Casos (Map2,:);

%Hacer Casos_HD para los que s� tienen enfermedades CV

%Graficar num de cigarros que fuman los que no tienen enfermedades
%cardiacas

%plot(Casos_NoHD(:,14), '*')
%bar(Casos_NoHD(:,14))

%15 (a�os como fumador), 17(historial de diabetes), 32(maxima frecuencia cardiaca alcanzada) 

%a�os como fumador
MediaAniosFumadores0HD = mean(Casos_NoHD(:,15));
MediaAniosFumadores1HD = mean(Casos_SiHD(:,15));
plot (Casos_NoHD(:,15), '*');
plot (Casos_SiHD(:,15), '*');

%historial familiar con afeccion en la arteria coronaria (si o no)
Map = Casos(:, 18)==1;
Map2= Casos(:, 18)~=1;

Casos_SiFamHist = Casos (Map, :);
Casos_NoFamHist = Casos (Map2, :);

U = unique(Casos(:,58));
SU = size(U);%numero de diferentes valores en el arreglo
Freqs = zeros(SU);%arreglo unidimensional del tama�o de SU
for c=1:SU(1)
    map=Casos_SiFamHist(:,58)==c-1;%cuenta los valores identicos al numero que se encuentra en la posicion "c" del arreglo U
    Freqs(c)=sum(sum(map));%suma todos los numeros positivos en el map
end
Frecuencias = [U , Freqs];%concatena los numeros mas su frecuencia
bar (Freqs);

SU2 = size(U);%numero de diferentes valores en el arreglo
Freqs2 = zeros(SU2);%arreglo unidimensional del tama�o de SU
%Contar la frecuencia de cada valor
for c=1:SU2(1)
    map=Casos_NoFamHist(:,58)==c-1;%cuenta los valores identicos al numero que se encuentra en la posicion "c" del arreglo U
    Freqs2(c)=sum(sum(map));%suma todos los numeros positivos en el map
end
Frecuencias2 = [U , Freqs2];%concatena los numeros mas su frecuencia
bar (Freqs2);

%maxima frecuencia cardiaca alcanzada

MediaFC0HD = mean(Casos_NoHD(:,32));
MediaFC1HD = mean(Casos_SiHD(:,32));

StdFC0HD = std (Casos_NoHD(:,32));
StdFC1HD = std (Casos_SiHD(:,32));

FC_NoHD = sort(Casos_NoHD(:,32));
FC_SiHD = sort(Casos_SiHD(:,32));
plot (FC_NoHD);
plot (FC_SiHD);